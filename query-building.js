const mongoose = require('mongoose')
const Building = require('./models/building')
const Room = require('./models/room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  // const rooms = await Room.findById('621793a823f9a60f6b5b2aa3')
  // console.log(rooms)
  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('---------------------------------------------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const bd = await Building.find({}).populate('room')
  console.log(JSON.stringify(bd))
}
main().then(() => {
  console.log('finish')
})
