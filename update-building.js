const mongoose = require('mongoose')
const Building = require('./models/building')
const Room = require('./models/room')
mongoose.connect('mongodb://localhost:27017/example')
async function main () {
  const educations = await Building.findById('621799fbdc1f6de9a568e673')
  const rooms = await Room.findById('621799fbdc1f6de9a568e677')
  const informatics = await Building.findById(rooms.building)
  console.log(rooms)
  console.log('---------------------------------------------------------')
  console.log(educations)
  console.log('---------------------------------------------------------')
  console.log(informatics)
  rooms.building = educations
  educations.room.push(rooms)
  informatics.room.pull(rooms)
  educations.save()
  rooms.save()
  informatics.save()
}
main().then(() => {
  console.log('finish')
})
